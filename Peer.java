
import java.io.*;
import java.net.*;
import java.util.concurrent.Semaphore;
import java.util.*;


public class Peer {

    static final String LOCAL_HOST_IP = "127.0.0.1";

    static final int MAX_PEER_ID = 256;
    static final int MIN_PEER_ID = 0;
    static final int UNINITIALISED_PEER = -1;

    static final int PORT_OFFSET = 51234;

    // CLASS VARIABLES *************************************************************************************************

    private DatagramSocket socketUDP;
    private ServerSocket socketTCP;

    private int thisPeerID;
    private Peers successors;
    private Peers predecessors;

    // CLASS METHODS ***************************************************************************************************

    // Constructor  : Peer Item
    public Peer(int peerID, int successor1ID, int successor2ID) {

        // Input checking
        assert (peerID <= MAX_PEER_ID || peerID >= MIN_PEER_ID);
        assert (successor1ID <= MAX_PEER_ID || successor1ID >= MIN_PEER_ID);
        assert (successor2ID <= MAX_PEER_ID || successor2ID >= MIN_PEER_ID);

        // @Adam Not sure if I should test whether a peer can have itself as a successor.
        // Comment on the forum says I shouldn't have to worry about this case. it is therefore presumably
        // invalid case.
        assert (peerID != successor1ID && peerID != successor2ID && successor1ID != successor2ID);

        //Initialisations

        // Assign the successor peers
        thisPeerID = peerID;

        successors = new Peers(successor1ID, successor2ID, true);
        predecessors = new Peers (UNINITIALISED_PEER, UNINITIALISED_PEER, false);

        // Starts the peer
        startPeer();
    }

    // This is where the action happens
    public void startPeer() {

        // Perform UDP monitoring
        try {
            //Create UDP socket
            socketUDP = new DatagramSocket( getThisNodePortNum() );

            // Create a PingServer thread
            PingServer scanner = new PingServer(socketUDP);
            scanner.start();

            // Create a PeerPinger thread
            PingClient heartbeat = new PingClient(socketUDP);
            heartbeat.start();

        } catch (SocketException e) {
            //e.printStackTrace();
            System.out.println("Error : Couldn't create datagram socket");
        }


        try {
            socketTCP = new ServerSocket(getThisNodePortNum());

            // Create a TCP scanner
            TCPServer serverScanner = new TCPServer(socketTCP);
            serverScanner.start();

        } catch (IOException e) {
            System.out.println("Error: Couldn't create server socket");
            e.printStackTrace();
        }

        Scanner in = new Scanner(System.in);
        ConsoleClient console = new ConsoleClient(in);
        console.start();


        }

    // PRIVATE INNER CLASS - TCPServer *********************************************************************************

    // TCP creates a new thread, and uses that to monitor the TCP port for incoming
    //  TCP commands and requests.
    private class TCPServer implements Runnable {

        // Private member fields --------------------------------------------
        private Thread t;
        private ServerSocket socket;
        private Socket connection;

        // Member methods --------------------------------------------

        // Constructor
        public TCPServer(ServerSocket socketTCP) {
            socket = socketTCP;
        }

        // Create new thread then start the new thread.
        public void start() {
            t = new Thread(this, "TCP Scanner thread");
            t.start();
        }

        // This is the function that performs the work of the thread.
        @Override
        public void run() {

            // Inspired by http://www.javaworld.com/article/2078809/java-concurrency/
            // java-concurrency-java-101-the-next-generation-java-concurrency-without-the-pain-part-1.html
            try {
                // In here we should repeatedly check the port for incoming connection requests.
                while(true) {

                    // Accept the connection
                    connection = socket.accept();

                    //Create new thread to process the connection.
                    Runnable r = new Runnable() {
                        @Override
                        public void run() {
                            try {
                                processConnection(connection);
                            } catch (IOException e) {
                                System.out.println("ERROR : Tried to connect to peer (maybe) but received error.");
                                e.printStackTrace();
                            } catch (InterruptedException e) {
                                System.out.println("ERROR : Was interrupted whilst trying to find peer.");
                                e.printStackTrace();
                            }
                        }
                    };

                    new Thread(r).start();

                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        // PROCESSING METHODS ------------------------------------------------------------------------------------------

        // This method receives the connection, and delegates to the appropriate method based on packet type.
        private void processConnection(Socket connection) throws IOException, InterruptedException {

            // GET THE DATA FROM THE PACKET

            // create read stream to get input
            BufferedReader inFromClient = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            PrintWriter outToClient = new PrintWriter(connection.getOutputStream(), true);

            PacketHandler p = new PacketHandler();

            // Get the packet as a string
            String clientPacket = p.getPacket(inFromClient);

            // DETERMINE WHAT KIND OF TCP PACKET IT IS
            String protocolType = p.parseProtocol(clientPacket);

            if ( protocolType.equals( "file_protocol")  ) {
//                System.out.println("We have detected a file protocol packet!");
                processFileProtocol(clientPacket, connection);
            } else if (protocolType.equals( "network_control") ) {
//                System.out.println("We have detected a network control packet!");
                processNetworkControlProtocol (clientPacket, connection, inFromClient, outToClient);
            } else {
                System.out.println("ERROR : UNKNOWN PACKET TYPE RECEIVED. IGNORING PACKET.");
            }
        }

        // FILE PROTOCOL METHODS -------------------------------------------------__------------------------------------

        // This method handles file request packets, and the response packet from a peer that has a file we requested.
        private void processFileProtocol(String packet, Socket connection) throws InterruptedException, IOException {

            PacketHandler p = new PacketHandler();

            // Get the type of file protocol packet. Can be : file_query_mode or file_possession_mode.
            String fileProtocolMode = p.parseFileProtocolMode(packet);

            // CASE : We receive a packet from our predecessor asking if we have a file or not.
            if ( fileProtocolMode.equals("file_query_mode") ) {
                processFileProtocolQueryMode(packet);
            }
            // CASE : We received a notification from a misc peer that they have the file we requested.
            else if ( fileProtocolMode.equals("file_possession_mode") ) {

                String filename = p.parseDataFromPacket("FILE_NAME", packet);
                int peerID = getPeerID(connection.getPort());

                // get server port from packet and convert to int
                int originatingPeerID = Integer.parseInt(p.parseDataFromPacket("FROM_ADDRESS", packet));

                System.out.println("Received a response message from peer " + originatingPeerID + ", which has the file "
                        + filename + ".");
            }
        }

        // Processes file protocol queries. ie a peer has asked us if we have the file, this is the function we run.
        // There are two main cases : we have the file, or we don't. In each case we send a packet to reflect that.
        private void processFileProtocolQueryMode(String packet) throws InterruptedException, IOException {

            // Create destination variables
            InetAddress serverIPAddress;
            int serverPort;
            String successMessage = "";
            Socket serverSocket;

            PacketHandler p = new PacketHandler();

            // Get the filename from the packet and hash it
            String filename = p.parseDataFromPacket("FILE_NAME", packet);
            int fileHash = hashFilename(filename);

            // CASE : We own the file - send a file possession packet to the requesting peer.
            if (weOwnThisFile(fileHash)) {

                // Setup socket for reply to originating peer
                serverIPAddress = InetAddress.getByName(LOCAL_HOST_IP);

                // get server port from packet and convert to int
                int originatingPeerID = Integer.parseInt(p.parseDataFromPacket("DESTINATION_ADDRESS", packet));
                serverPort = getPortNum(originatingPeerID);

                packet = p.modifyPacketData(packet, "FILE_PROTOCOL_MODE", "file_possession_mode");
                packet = p.modifyPacketData(packet, "FROM_ADDRESS", String.valueOf(thisPeerID));

                // create socket which connects to server
                serverSocket = new Socket(serverIPAddress, serverPort);

                successMessage = "File " + filename + " is here.\n" +
                        "A response message, destined for peer " + originatingPeerID + ", has been sent.";

            }
            // CASE : We do not own the file - Pass packet onto our successor.
            else {

                // Setup socket for forwarding of request - Destination will be the our closest successor peer
                serverIPAddress = InetAddress.getByName(LOCAL_HOST_IP);
                serverPort = getPortNum(successors.getClosestPeer());
                serverSocket = new Socket(serverIPAddress, serverPort);

                successMessage = "File " + filename +
                        " is not stored here.\nFile request message has been forwarded to my successor.";

            }

            // WRITE TO SOCKET AND SEND!
            DataOutputStream outToServer = new DataOutputStream(serverSocket.getOutputStream());
            outToServer.writeBytes (packet);
            outToServer.close();
            serverSocket.close();

            // Print successMessage
            System.out.println(successMessage);
        }

        private boolean weOwnThisFile(int fileHash ) throws InterruptedException {

            // We own the file if :
            // - The fileHash is equal to us
            // - The fileHash is greater than us, AND we are greater than our successor
            // - The fileHash is less than us, AND it is less than our successor
            // - The fileHash is greater than us AND less than our successor

            if (fileHash == thisPeerID) {
                return true;
            }

            if (    (fileHash > thisPeerID)    &&    (thisPeerID > successors.getClosestPeer())    ) {
                return true;
            }

            if (    (fileHash < thisPeerID)    &&    (fileHash < successors.getClosestPeer())     ) {
                return true;
            }

            if (    (fileHash > thisPeerID)    &&    (fileHash < successors.getClosestPeer())     ) {
                return true;
            }

            return false;
        }

        private int hashFilename (String filename) {

            // Get the int equivalent of the first char
            int fn0 = Character.getNumericValue(filename.charAt(0));
            int fn1 = Character.getNumericValue(filename.charAt(1));
            int fn2 = Character.getNumericValue(filename.charAt(2));
            int fn3 = Character.getNumericValue(filename.charAt(3));

            int filenameIntEquivalent  = (fn0 * 1000) + (fn1 * 100) + (fn2 * 10) + fn3 + 1;
            int remainder = filenameIntEquivalent % 256;
            return remainder;
        }


        // NETWORK CONTROL PROTOCOL METHODS ----------------------------------_-----------------------------------------

        // This function handles NetworkControl protocol packets, ie departure notifications and peer info requests.
        private void processNetworkControlProtocol(String packet, Socket connection, BufferedReader inFromClient,
                                                   PrintWriter outToClient) throws IOException, InterruptedException {
            PacketHandler p = new PacketHandler();

            String networkControlMode = p.parseDataFromPacket("NETWORK_CONTROL_MODE", packet);
            if ( networkControlMode.equals("departure_notification") ) {

                // Determine the departing node ID
                int departingNodeAddress = Integer.parseInt(p.parseDataFromPacket("DEPARTING_NODE_ADDRESS", packet));

                // Print notification
                System.out.println("Peer " + departingNodeAddress + " will depart from the network.");

                // Send receipt of notification by sending a departure_acknowledgement
                outToClient.println("PROTOCOL:network_control");
                outToClient.println("NETWORK_CONTROL_MODE:departure_acknowledgement");
                outToClient.println("EOT"); // End of Transmission signal
                outToClient.flush();

                // NOW REPLACE OUR SUCCESSORS BASED ON THAT INFORMATION.
                int successorClosest = Integer.parseInt(p.parseDataFromPacket("SUCCESSOR_CLOSEST", packet));
                int successorBackup = Integer.parseInt(p.parseDataFromPacket("SUCCESSOR_BACKUP", packet));

                // If it used to be our backup, then we  want to remove it,
                // and only add its closest successor.

                // CASE : If departing node was our backup; remove it, then add ONLY it's closest successor as a replacement
                if (successors.backupPeer == departingNodeAddress ) {
                    successors.removePeer(departingNodeAddress);
                    //predecessors.removePeer(departingNodeAddress);
                    successors.addPeer(successorClosest);

                }
                // CASE : If departing node was our closest node, remove it, then add both of it's successors.
                else {
                    successors.removePeer(departingNodeAddress);
                    //predecessors.removePeer(departingNodeAddress);
                    successors.addPeer(successorClosest);
                    successors.addPeer(successorBackup);

                }

                System.out.println("My first successor is now peer " + successors.getClosestPeer() + "." + "\n"
                        + "My second successor is now peer " + successors.getBackupPeer() + ".");

            } else if (networkControlMode.equals("successor_request")){

                //System.out.println("Have been asked who my successor is.");

                // Send receipt of notification by sending a departure_acknowledgement
                outToClient.println("PROTOCOL:network_control");
                outToClient.println("NETWORK_CONTROL_MODE:successor_reply");
                outToClient.println("SUCCESSOR:" + successors.getClosestPeer() );
                outToClient.println("EOT"); // End of Transmission signal
                outToClient.flush();

            }

        }
    }

    // PRIVATE INNER CLASS - ConsoleClient *****************************************************************************

    // ConsoleClient creates a new thread, and uses that to monitor the console for user commands
    private class ConsoleClient implements Runnable {

        static final String CONSOLE_REQUEST = "REQUEST";
        static final String CONSOLE_QUIT = "QUIT";

        // Private member fields ---------------------------------------------------------------------------------------
        private Thread t;
        private Scanner in;
        private String command;

        // Member methods ----------------------------------------------------------------------------------------------

        // Constructor
        public ConsoleClient(Scanner in) {
            this.in = in;
        }

        // Create new thread then start the new thread.
        public void start() {
            t = new Thread(this, "Console Scanner thread");
//            System.out.println("Console Scanner starting : " + t);
            t.start();
        }

        // This is the function that performs the work of the thread.
        @Override
        public void run() {

            // Inspired by http://www.javaworld.com/article/2078809/java-concurrency/
            // java-concurrency-java-101-the-next-generation-java-concurrency-without-the-pain-part-1.html
                while (true) {
                    // In here we should repeatedly check the console for commands
                    command = in.nextLine();
                    // Create new thread to handle incoming packet.
                    Runnable r = new Runnable() {
                        @Override
                        public void run() {
                            try {
                                processCommand(command);
                            } catch (IOException e) {
                                System.out.println("Tried to send a packet, but server could not be found.");
                                //e.printStackTrace();
                            } catch (InterruptedException e) {
                                System.out.println("ERROR : We were interrupted for some reason (" + e + ")");
                                //e.printStackTrace();
                            }
                        }
                    };
                    new Thread(r).start();
                }

        }

        // PROCESSING METHODS ------------------------------------------------------------------------------------------

        public void processCommand(String command) throws IOException, InterruptedException {

            // Some commands have data (e.g. request 2000). Separate the command from the data.
            String commandType = getCommandType(command);
            String commandData = getCommandData(command);

            // FILE REQUEST
            if ( commandType.equals(CONSOLE_REQUEST) ) {

                if (! processFileRequest(commandData) ) {
                    System.out.println("Unable to process REQUEST. File name is invalid.");
                }
            }
            // QUIT REQUEST
            else if ( commandType.equals(CONSOLE_QUIT) )  {
                processQuit();
            } else if (commandType.equals("DIE")) {
                // Added for testing, as I can't close an xterm on my machine. :S
                System.exit(0);
            }
            else {
                System.out.println("ERROR : AN UNKNOWN TYPE OF CONSOLE COMMAND WAS RECEIVED.");
            }
        }

        // FILE REQUEST METHODS ----------------------------------------------------------------------------------------

        // Returns false if filename is invalid
        private boolean processFileRequest(String filename) throws InterruptedException, IOException {

            //Inspired by CSE code :http://www.cse.unsw.edu.au/~cs3331/15s2/Assignment/TCPClient.java

            // Check that file name is valid
            if (! fileNameIsValid(filename) ) {
                return false;
            }

            // Create socket connection to successor to ask for file
            InetAddress serverIPAddress = InetAddress.getByName(LOCAL_HOST_IP);
            int serverPort = getPortNum(successors.getClosestPeer());
            Socket clientSocket = new Socket(serverIPAddress, serverPort);

            // Prepare the packet data
            String destinationAddress = Integer.toString(thisPeerID);

            // Prepare the packet
            String fileRequestPacket = "PROTOCOL:file_protocol\n";
            fileRequestPacket += "FILE_PROTOCOL_MODE:file_query_mode\n";
            fileRequestPacket += "DESTINATION_ADDRESS:" + destinationAddress + "\n";
            fileRequestPacket += "FROM_ADDRESS:" + destinationAddress + "\n";
            fileRequestPacket += "FILE_NAME:" + filename;

            // write to server
            DataOutputStream outToServer = new DataOutputStream(clientSocket.getOutputStream());
            outToServer.writeBytes(fileRequestPacket );
            outToServer.close();
            clientSocket.close();

            System.out.println("File request message for " + filename + " has been sent to my successor.");
            return true;

        }

        // Checks if file name is valid (ie length is 4 and only numbers)
        private boolean fileNameIsValid (String filename){

            if (filename.length() != 4) {
                return false;
            }

            if (! filename.matches("\\d++")){
                return false;
            }
            return true;
        }

        // QUIT COMMAND METHODS ----------------------------------------------------------------------

        private void processQuit() throws InterruptedException, IOException {

            //Send a NETWORK_CONTROL - LEAVE_NOTIFICATION TCP packet to our two predecessors.
//            // Wait for response from both of them.
//            // Force main function to end.

            // It is possible that we haven't yet received notification of our predecessors, so we wait until we have.

            System.out.print("Notifying my peers of my departure..");
            while(! notifyPeerOfQuit(predecessors.getClosestPeer(),successors) ){};
            System.out.print("..");
            while(! notifyPeerOfQuit(predecessors.getBackupPeer(),successors)) {};
            System.out.print(".. GOODBYE.\n");

            System.exit(0);

        }

        private boolean notifyPeerOfQuit(int peerID, Peers successors) throws IOException, InterruptedException {

            // Code below influenced by : http://java.icmc.usp.br/books/ooc/html/
            // distributed_programming_sockets_and_streams.html

            if( (successors.getBackupPeer() == -1) || (successors.getClosestPeer() == -1) ) {
                System.out.println("ERROR : successors supplied were not valid");
                return false;
            }

            // Get details for socket connection
            InetAddress serverIPAddress = InetAddress.getByName(LOCAL_HOST_IP);
            int serverPort = getPortNum(peerID);

            // Get the packet data for the connection
            String departingNodeAddress = Integer.toString(thisPeerID);

            try {
                // Create network connection with In&Out streams
                Socket predecessorSocket = new Socket(serverIPAddress, serverPort);
                PrintWriter os = new PrintWriter(predecessorSocket.getOutputStream(), true);
                BufferedReader is = new BufferedReader(new InputStreamReader(predecessorSocket.getInputStream()));

                // Send a departure notification packet
                os.println("PROTOCOL:network_control");
                os.println("NETWORK_CONTROL_MODE:departure_notification");
                os.println("DEPARTING_NODE_ADDRESS:" + departingNodeAddress);
                os.println("SUCCESSOR_CLOSEST:" + successors.getClosestPeer());
                os.println("SUCCESSOR_BACKUP:" + successors.getBackupPeer());
                os.println("EOT"); // EOT = end of transmission
                os.flush();

                PacketHandler p = new PacketHandler();

                // Receive reply packet
                String reply = p.getPacket(is);

                // Check that reply is actually an acknowledgement of departure.
                String confirmation = p.parseDataFromPacket("NETWORK_CONTROL_MODE", reply);
                if (! confirmation.equals("departure_acknowledgement")) {
                    System.out.println("ERROR : Received a reply we weren't expecting.");
                    return false;
                }

                // Close connection
                os.close();
                is.close();
                predecessorSocket.close();

            } catch (UnknownHostException e) {
                System.err.println("ERROR : Could not connect to localhost");
                throw e;
            } catch (IOException e) {
                System.err.println("ERROR : Couldn't get I/O for the connection to localhost. Error message : " + e);
                throw e;
            }
            return true;

        }

        // UTILITY METHODS ----------------------------------------------------------------------


        // Returns just the command type of a command string. ie "REQUEST" from "REQUEST 3000"
        private String getCommandType(String s) {
            String sanitizedCommand;
            String rawCommand = s.toUpperCase();

            // Assume there is no spaces
            sanitizedCommand = rawCommand;

            // CASE : There is command, SPACE, then data
            int i = rawCommand.indexOf(' ');
            if (i != -1) {
                // If space, just take the first word.
                sanitizedCommand = rawCommand.substring(0, i);
            }
            return sanitizedCommand;
        }

        // Returns just the data of a command string. ie "3000" from "REQUEST 3000"
        private String getCommandData(String s) {
            String sanitizedData;
            String rawInput = s;

            // Assume there is no spaces, therefore no data
            sanitizedData = "";

            // CASE : There is command, SPACE, then data
            int i = rawInput.indexOf(' ');
            if (i != -1) {
                // If space, just take the first word.
                //sanitizedCommand = rawCommand.substring(0, i);
                sanitizedData = rawInput.substring(i);
            }
            return sanitizedData.trim();
        }


//        // Closes the socket and ends thread.
//        public void stop () {
////            //Close socket
////            if (in.isConnected()) {
////                in.close();
////            }
//            // End thread
//            t.interrupt();
//        }
    }


    // PingServer creates a new thread, and uses that to monitor the UDP port for incoming
    // ping requests and  ping responses.
    // @Adam Could this be called the PingServer?
    // PING SERVER
    private class PingServer implements Runnable {

        static final byte PING_REQUEST = 0b00000001;
        static final byte PING_RESPONSE = 0b00000010;

        static final int PING_PACKET_SIZE = 4;
        static final int PINGHEADER_TYPE = 0;

        static final int BUFFER_SIZE = 4;


        // Private member fields --------------------------------------------
        private Thread t;
        private DatagramSocket socket;
        private CircularBuffer predecessorPingBuffer;
        private CircularBuffer successorPingBuffer;
        DatagramPacket request;


        // Member methods --------------------------------------------


        // Constructor
        public PingServer(DatagramSocket socketUDP) {
            socket = socketUDP;
            // Will be used to store the last four pings to detect successor/predecessor departures
            predecessorPingBuffer = new CircularBuffer(BUFFER_SIZE);
            successorPingBuffer = new CircularBuffer(6);

        }

        // Create new thread then start the new thread.
        public void start() {
            t = new Thread(this, "UDP Scanner thread");
            //System.out.println("UDP Scanner starting : " + t);
            t.start();
        }

        // This is the function that performs the work of the thread.
        @Override
        public void run() {
            // Inspired by http://www.javaworld.com/article/2078809/java-concurrency/
            // java-concurrency-java-101-the-next-generation-java-concurrency-without-the-pain-part-1.html

            try {
                // In here we should repeatedly check the port for incoming UDP packets.
                while (true) {
                    // Create blank request for input
                    request = new DatagramPacket(new byte[1024], 1024);

                    // Block until the host receives a UDP packet.
                    socket.receive(request);

                    // Create new thread to handle incoming packet.
                    Runnable r = new Runnable() {
                        @Override
                        public void run() {
                            try {
                                processPacket(request);
                            } catch (IOException e) {
                                System.out.println("Unable to process incoming packet. Packet discarded.");
                                e.printStackTrace();
                            } catch (InterruptedException e) {
                                System.out.println("We were interrupted whilst trying to process a ping packet");
                                e.printStackTrace();
                            }
                        }
                    };
                    new Thread(r).start();
                }
//            } catch(InterruptedException e) {
//                System.out.println("my thread interrupted");
            } catch (IOException e) {
                System.out.println("Error : IO exception with the UDP socket. It is likely that the socket was not open.");
                e.printStackTrace();
            }
        }

        public void processPacket(DatagramPacket request) throws IOException, InterruptedException {

            // Get the datagram
            byte[] buf = request.getData();

            // Check that packet is valid before using as array below
            if ( buf.length == 0) {
                System.out.println("Error : Empty UDP packet received. Discarding packet.");
                return;
            }

            // PING REQUEST
            if ( buf[PINGHEADER_TYPE] == PING_REQUEST ) {

                // Get data about the sender
                int clientPort = request.getPort();
                int clientID = getPeerID(request.getPort());

                System.out.println("A ping request message received from Peer " + clientID + "." );

                // Setup reply message buffer and set ping type to response
                byte[] replyBuf = new byte[PING_PACKET_SIZE];
                replyBuf[PINGHEADER_TYPE] = PING_RESPONSE;

                // Create reply packet and send
                InetAddress clientHost = InetAddress.getByName("127.0.0.1");
                DatagramPacket replyPacket = new DatagramPacket(replyBuf, replyBuf.length, clientHost, clientPort);
                socket.send(replyPacket);

                // Check (and add if necessary) new predecessors. Including initial AND replacement predecessors.
                checkForNewPredecessors(clientID);

                // PING RESPONSE
            } else if (buf[PINGHEADER_TYPE] == PING_RESPONSE )  {


                int clientID = getPeerID(request.getPort());
                System.out.println("A ping response message was received from Peer " + clientID + "." );

                //checkIfSuccessorsAreAlive(clientID);

                // Perhaps in the future we want to record the time of receiving the ping.
            } else {
                System.out.println("ERROR : AN UNKNOWN TYPE OF PING WAS RECEIVED.");
            }

            // We have finished processing the ping. Now return.
        }


        // @Adam This could be deleted
        private void checkIfSuccessorsAreAlive(int incomingPeer) throws InterruptedException, IOException {
            // Keep a record of the last series of ping replies.
            successorPingBuffer.add(incomingPeer);

            // Exit if we haven't sampled at least 4 pings. We want to give peers a chance to get started.
            if (! successorPingBuffer.isFilled()) {
                //System.out.println("Buffer is not filled yet.");
                return;
            }

            // Get our current successor peers
            int closestPeer = successors.getClosestPeer();
            int backupPeer = successors.getBackupPeer();


            // Check the succcessorPingBuffer to see if a peer has sent fewer pings (which suggests it has left unexpectedly)
            int numPingsFromClosest =  successorPingBuffer.numOccurances(closestPeer);
            int numPingsFromBackup =  successorPingBuffer.numOccurances(backupPeer);



            boolean onePeerHasDied = successorPingBuffer.duplicatesInLast(4);

            // Detect when the last 4 pings where all the same.


            if (numPingsFromBackup < 1 )  {
                System.out.println("Have detected that our second successor " + backupPeer +" is not responding.");
                System.out.print( "Last pings : ");
                for ( int i = 0 ; i < successorPingBuffer.bufferSize ; i++ ) {
                    System.out.print( successorPingBuffer.get(i) + " ");
                }
                System.out.print( "\n");
                System.out.println("Successor ping count (Closest : " + numPingsFromClosest + ")   (Backup : "  + numPingsFromBackup + ")" );

            }

            if (numPingsFromClosest < 1 ) {
                System.out.println("Have detected that our first successor " + closestPeer +" is not responding.");
                System.out.print( "Last pings : ");
                for ( int i = 0 ; i < successorPingBuffer.bufferSize ; i++ ) {
                    System.out.print( successorPingBuffer.get(i) + " ");
                }
                System.out.print( "\n");
                System.out.println("Successor ping count (Closest : " + numPingsFromClosest + ")   (Backup : " + numPingsFromBackup + ")");
            }


            // CASE : Closest peer has stopped sending
             if (numPingsFromClosest < 2  ) {
//                System.out.println("Removed closest predecessor :" + closestPeer  + " (a new predecessor added : " + incomingPeer + ")");

                 // Make backup successor the closest successor, by removing the currentClosest successor.
                 successors.removePeer(closestPeer);

                 // Ask our (new) closest successor for it's closest successor, so we can make it our backup.
                 int newBackupSuccessor = requestClosestSuccessor(successors.getClosestPeer());

                 System.out.println("Successor " + closestPeer + " has stopped responding. Will be replaced with " + newBackupSuccessor);

                 successors.addPeer(newBackupSuccessor);
                 successorPingBuffer.clear();
            }
            // CASE : Backup has stopped sending
            if ( numPingsFromBackup < 1 ) {

                 // Remove the backupPeer.
                 successors.removePeer(backupPeer);

                 // Ask our (new) closest successor for it's closest successor, so we can make it our backup.
                 int newBackupSuccessor = requestClosestSuccessor(successors.getClosestPeer());

                 System.out.println("Successor " + backupPeer + " has stopped responding. Will be replaced with " + newBackupSuccessor);

                 successors.addPeer(newBackupSuccessor);
                successorPingBuffer.clear();

            }
             else {
                 // Normal amount of ping replies received.
            }

        }


        // @Adam This could be deleted.
        private int requestClosestSuccessor(int peerID) throws IOException {
            // Create a TCP socket, connect to the specified peer, send a successorRequestPacket, collect response,
            // parse response, return value.

            int retVal = 0;

            // Get details for socket connection
            InetAddress serverIPAddress = InetAddress.getByName(LOCAL_HOST_IP);
            int serverPort = getPortNum(peerID);

            // Get the packet data for the connection
            String successorNodeAddress = Integer.toString(thisPeerID);

            try {
                // Create network connection with In&Out streams
                Socket predecessorSocket = new Socket(serverIPAddress, serverPort);
                PrintWriter os = new PrintWriter(predecessorSocket.getOutputStream(), true);
                BufferedReader is = new BufferedReader(new InputStreamReader(predecessorSocket.getInputStream()));

                // Send a departure notification packet
                os.println("PROTOCOL:network_control");
                os.println("NETWORK_CONTROL_MODE:successor_request");
                os.println("EOT"); // EOT = end of transmission
                os.flush();

                PacketHandler p = new PacketHandler();

                // Receive reply packet
                String reply = p.getPacket(is);

                // Check that reply is actually an acknowledgement of departure.
                String confirmation = p.parseDataFromPacket("NETWORK_CONTROL_MODE",reply);
                if (! confirmation.equals("successor_reply")) {
                    System.out.println("ERROR : Received a reply we weren't expecting.");
                    throw new IOException("Were expecting a reply with successor info, but did not receive it.");
                }

                String successor = p.parseDataFromPacket("SUCCESSOR", reply);

                retVal = Integer.parseInt(successor);

                // Close connection
                os.close();
                is.close();
                predecessorSocket.close();

            } catch (UnknownHostException e) {
                System.err.println("ERROR : Could not connect to localhost");
                throw e;
            } catch (IOException e) {
                System.err.println("ERROR : Couldn't get I/O for the connection to localhost. Error message : " + e);
                throw e;
            }
            return retVal;

        }



        // This code takes incoming pings and saves them as predecessors. Can also adapt to a changed predecessor.
        private void checkForNewPredecessors(int incomingPeer) throws InterruptedException {
            // Keep a record of the last series of pings.
            predecessorPingBuffer.add(incomingPeer);

            // Get our current peers so we can compare against the incomingPeer.
            int closestPeer = predecessors.getClosestPeer();
            int backupPeer = predecessors.getBackupPeer();


            // CASE : We don't know all our predecessors yet - So definitely add this new predecessor.
            if ( (closestPeer == UNINITIALISED_PEER) || (backupPeer == UNINITIALISED_PEER) ) {
                //System.out.println("Detected a NEW predecessor :" + incomingPeer);
                predecessors.addPeer(incomingPeer);
            }
            // CASE : We have detected a new predecessor.
            else if (closestPeer != incomingPeer && backupPeer != incomingPeer ) {

//                System.out.println("Detected a REPLACEMENT peer :" + incomingPeer);
                // Check the predecessorPingBuffer for which peer has sent fewer pings (which suggests it has died)
                int numPingsFromClosest =  predecessorPingBuffer.numOccurances(closestPeer);
                int numPingsFromBackup =  predecessorPingBuffer.numOccurances(backupPeer);

                // CASE : Backup has stopped sending
                if ( numPingsFromBackup < numPingsFromClosest ) {
                    //System.out.println("Removed backup predecessor :" + backupPeer + " (a new predecessor added :" + incomingPeer + ")");

                    predecessors.removePeer(backupPeer);
                    predecessors.addPeer(incomingPeer);
                }
                // CASE : Closest peer has stopped sending
                else if (numPingsFromBackup > numPingsFromClosest ) {
                    //System.out.println("Removed closest predecessor :" + closestPeer  + " (a new predecessor added : " + incomingPeer + ")");

                    predecessors.removePeer(closestPeer);
                    predecessors.addPeer(incomingPeer);
                } else {
                    //System.out.println("ERROR : we detected a new predecessor, but we can't yet determine which peer it belongs to.");
                }

            } else {
//                System.out.println("We did not detect a new peer.");
            }
        }

        private class CircularBuffer {
            private int[] buffer;
            private int bufferIndex;



            private int bufferSize;
            private int fillCount;

            public CircularBuffer(int bufferSize) {
                buffer = new int[bufferSize];
                bufferIndex = -1;
                this.bufferSize = bufferSize;
                fillCount = 0;
            }

            public void add(int number ) {
                // Move the index to the next empty/victim spot in buffer
                bufferIndex = (bufferIndex +1) % bufferSize;
                buffer[bufferIndex] = number;

                // Allows us to determine if the buffer still has empty slots.
                if ( fillCount < bufferSize){
                    fillCount++;
                }

            }

            public void clear(){
                buffer = new int[bufferSize];
                bufferIndex = -1;
                fillCount = 0;
            }


            // Checks if the last num in the circular buffer are duplicates of each other.
            public boolean duplicatesInLast (int num) {
                int previous = 0 ;
                for (int i = 0 ; i < num && i < bufferSize ; i++ ) {
                    if (i == 0 ){
                        previous = get(i);
                        continue;
                    }

                    if (previous != get(i) ) {
                        return false;
                    }
                }
                return true;
            }

            public int getBufferSize() {
                return bufferSize;
            }

            public boolean isFilled () {
                return fillCount == bufferSize;
            }

            // get(0) gets the last added value.
            public int get(int howFarBack) {
                int getIndex = (Math.abs(bufferIndex - howFarBack)) % bufferSize;
                return buffer[getIndex];
            }

            public boolean contains (int number) {
                return (numOccurances(number) != 0);
            }

            public int numOccurances (int number) {
                int count = 0;

                for (int i = 0 ; i < bufferSize ; i++ ){
                    if ( get(i) == number ) {
                        count++;
                    };
                }
                return count;
            }

        }

//        // Closes the socket and ends thread.
//        public void stop () {
//            //Close socket
//            if (socket.isConnected()) {
//                socket.close();
//            }
//            // End thread
//            t.interrupt();
//        }
    }


    // PingClient creates a new thread, so that it can ping it's peers.
    private class PingClient implements Runnable {

        static final byte PING_REQUEST = 0b00000001;
        static final byte PING_RESPONSE = 0b00000010;

        static final int PING_PACKET_SIZE = 4;
        static final int PINGHEADER_TYPE = 0;

        static final int PING_INTERVAL_TIME = 20000;

        // Private member fields --------------------------------------------
        private Thread t;
        private DatagramSocket socket;

        // Member methods --------------------------------------------

        // Constructor
        public PingClient(DatagramSocket socketUDP) {
            socket = socketUDP;
        }

        public void start() {
            t = new Thread(this, "PingClient thread");
//            System.out.println("PingClient starting : " + t);
            t.start();
        }

        @Override
        public void run() {
            // In here we should ping both peers, then sleep, then do it again!
            try {
                while (true) {
                    pingSuccessors();
                    Thread.sleep(PING_INTERVAL_TIME);
                }
            } catch (InterruptedException e) {
                System.out.println("PingClient was interrupted for some reason");
                //e.printStackTrace();
            }
        }

        private void pingSuccessors() throws InterruptedException {

            // Get the successors we need to ping.
            int closestSuccessor = successors.getClosestPeer();
            int backupSuccessor = successors.getBackupPeer();

            if (closestSuccessor == UNINITIALISED_PEER || backupSuccessor == UNINITIALISED_PEER ) {
                System.out.println("ERROR : Tried to ping a successor without being told a valid successor at runtime");
            }

            // Ping closest successor
            if ( closestSuccessor != UNINITIALISED_PEER ) {
                try {
                    sendPingRequest(closestSuccessor);
                } catch (IOException e) {
                    System.out.println("ERROR : IO Exception. Tried to send ping request to successor 1. Maybe an unopened port.");
                }
            }

            // Ping the backup successor
            if ( backupSuccessor != UNINITIALISED_PEER ) {
                try {
                    sendPingRequest(backupSuccessor);
                } catch (IOException e) {
                    System.out.println("ERROR : IO Exception. Tried to send ping request to successor 2. Maybe an unopened port.");
                }
            }
        }

        private void sendPingRequest(int peerID) throws IOException {

            // Get data about destination
            int peerPortNumber = getPortNum(peerID);

            // Setup reply message buffer and set ping type to response
            byte[] replyBuf = new byte[PING_PACKET_SIZE];
            replyBuf[PINGHEADER_TYPE] = PING_REQUEST;

            // Create reply packet and send
            InetAddress clientHost = InetAddress.getByName("127.0.0.1");
            DatagramPacket replyPacket1 = new DatagramPacket(replyBuf, replyBuf.length, clientHost, peerPortNumber);
            socket.send(replyPacket1);
        }
    }



    private class PacketHandler {

        public PacketHandler () {

        }


        // Reads each line of the packet from a BufferedReader and returns a string of the packet
        private String getPacket(BufferedReader inFromClient) throws IOException {
            String clientPacket = "";
            for (String newLine = inFromClient.readLine() ; newLine != null ; newLine = inFromClient.readLine()) {

                if(newLine.equals("EOT")){
                    break;
                }

                // Add line breaks after the first line
                if (clientPacket.length() > 0 ) {
                    clientPacket += '\n';
                }
                clientPacket += newLine;
            }
            return clientPacket;
        }

        // This replaces the data of the specified field in a packet
        private String modifyPacketData (String packet, String dataField, String newData) {
            // Split each line into a separate array item
            String[] oldPacketSplit = packet.split("\n");

            String newPacket = "";

            // Go through each line
            for  (int i = 0 ; i < oldPacketSplit.length ; i++ ) {

                // Determine the field and the data for each line
                String currentLine = oldPacketSplit[i];
                String[] splitLine = currentLine.split(":");

                String currentField = splitLine[0].toUpperCase();
                String currentData = splitLine[1];

                // If we find the matching field, we are done.
                if (currentField.equals( dataField )){
                    currentLine = currentField + ":" + newData;
                }
                newPacket += currentLine ;
                if (i != (oldPacketSplit.length -1) ) {
                    newPacket += "\n";
                }
            }
            return newPacket;
        }

        // PACKET PARSING UTILITIES ----------------------------------------------------------------------

        // This extracts the data from a packet when given the field. Returns "" if not found.
        private String parseDataFromPacket (String dataField, String packet) {

            // Sanitize input
            dataField.toUpperCase();

            // Split each line into a separate array item
            String[] clientPacketList = packet.split("\n");

            // Go through each line
            for  (int i = 0 ; i < clientPacketList.length ; i++ ) {

                // Determine the field and the data for each line
                String currentLine = clientPacketList[i];
                String[] splitLine = currentLine.split(":");
                String currentField = splitLine[0].toUpperCase();
                String currentData = splitLine[1];

                // If we find the matching field, we are done.
                if (currentField.equals( dataField )){
                    return currentData;
                }
            }
            // We found nothing, Return nothing.
            return "";

        }

        private String parseProtocol(String packet) {
            return parseDataFromPacket ( "PROTOCOL", packet);
        }

        private String parseFileProtocolMode(String packet) {
            return parseDataFromPacket ( "FILE_PROTOCOL_MODE", packet);
        }



    }

    // Peers Class : Stores the neighbouring peers.
    private class Peers {

        private final Semaphore lock;

        private int closestPeer;
        private int backupPeer;
        // Used to differentiate between storing successor peers and predecessor peers
        private boolean directionIsUp;

        public Peers(int closestPeer, int backupPeer, boolean directionIsUp) {
            this.lock = new Semaphore(1,true);

            try {
                lock.acquire();
                this.directionIsUp = directionIsUp;

                this.closestPeer = UNINITIALISED_PEER;
                this.backupPeer = UNINITIALISED_PEER;

                addPeerInternal(closestPeer);
                addPeerInternal(backupPeer);

                lock.release();

                //            this.closestPeer = closestPeer;
                //            this.backupPeer = backupPeer;

            } catch (InterruptedException e) {
                System.out.println("EXTREME ERROR : Could not obtain lock for peers object that was just created. Very odd. Possibly interrupted.");
                e.printStackTrace();
            }
        }

        public int getClosestPeer() throws InterruptedException {
            lock.acquire();
            try {
                return closestPeer;
            } finally {
                lock.release();
            }
        }

        public int getBackupPeer() throws InterruptedException  {
            lock.acquire();
            try {
                return backupPeer;
            } finally {
                lock.release();
            }
        }

        public boolean addPeer(int peer) throws InterruptedException {
            lock.acquire();
            boolean result = addPeerInternal(peer);
            lock.release();
            return result;
        }

        private boolean addPeerInternal (int peer)  {

            if (peer == UNINITIALISED_PEER ) {return false;}
            if (peer == this.backupPeer || peer == this.closestPeer) {return false;}

            // CASE : No existing peers,
            if (closestPeer == UNINITIALISED_PEER) {
                this.closestPeer = peer; //then add peer as closest
            }
            // CASE : More close than current closestPeer
            else if (isCloser( peer, closestPeer )) {
                this.backupPeer = closestPeer; // Kickout the backup peer, Make previous closest the new backup
                this.closestPeer = peer; //Make new peer the closest

            }
            // CASE : If we have no backup peer, and this peer is not closer than our closest
            else if (backupPeer == UNINITIALISED_PEER) {
                this.backupPeer = peer; // Then we will accept it as our backup
            }
            // CASE : More close than the backup peer
            else if ( isCloser(peer, backupPeer)) {
                this.backupPeer = peer; // Kickout the old backup. Make this new peer the backup.
            }
            // ELSE : There is no use for this peer if it is not closer than our current peers.
            else {
//                System.out.println("DID NOT ADD PEER AS IT DID NOT MATCH CONDITIONS");
//                System.out.println("Current peers are : " + backupPeer + " and " + closestPeer );
//                System.out.println("attempted to add : " + peer);
                return false;
            }
            return true;
        }

        public void removePeer (int peer) throws InterruptedException {
            lock.acquire();
            if ( peer == closestPeer ) {
                this.closestPeer = this.backupPeer;
                this.backupPeer = UNINITIALISED_PEER;
            } else if (peer == backupPeer) {
                this.backupPeer = UNINITIALISED_PEER;
            } else {
                //System.out.println("ERROR : Was asked to remove a peer that doesn't exist.");
                lock.release();
                throw new RuntimeException();
            }
            lock.release();
        }

        // Returns true if a isCloser than b.
        // This function takes into account storing successors or predecessors.
        // Assumes lock is already acquired
        public boolean isCloser (int a, int b) {

            boolean result;

            // If both are larger (or smaller!) than the peer, the smallest number is closest to the peer
            if ( (a > thisPeerID) && (b > thisPeerID) || (a < thisPeerID) && (b < thisPeerID) ) {
                result =  a < b;
            } else {
                // If one is larger and the other smaller than the current peer, the largest value is always the closest.
                result =  a > b;
            }

            // The result is flipped if we are looking for predecessors, not successors.
            if (! directionIsUp ){
                result = !result;
            }
            return result;
        }
    }

    // HELPER FUNCTIONS ********************************************************
    public int getThisNodePortNum () {
        return thisPeerID + PORT_OFFSET;
    }

    public int getPortNum (int peerID) {
        return peerID + PORT_OFFSET;
    }

    public int getPeerID (int portNum) {
        return portNum - PORT_OFFSET;
    }

}

