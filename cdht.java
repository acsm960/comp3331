public class cdht {

    public static void main(String[] args) {

        if (args.length != 3) {
            System.out.println("ERROR : No commandline argument");
            System.exit(0);
        }

        int peerNumber = Integer.parseInt(args[0]);
        int neighbour = Integer.parseInt(args[1]);
        int neighbour2 = Integer.parseInt(args[2]);

        Peer t1 = new Peer(peerNumber, neighbour, neighbour2);
    }
}

